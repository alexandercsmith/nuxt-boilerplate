# COMPONENTS

* The components directory contains your Vue.js Components.
* Nuxt.js doesn't supercharge these components.
* [Nuxt.js - Vue.js Components](https://vuejs.org/v2/guide/components.html)
