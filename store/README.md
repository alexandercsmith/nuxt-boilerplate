# STORE

* This directory contains your Vuex Store files.
* Vuex Store option is implemented in the Nuxt.js framework.
* Creating a file in this directory automatically activates the option in the framework.
* [Nuxt.js - Vuex Store](https://nuxtjs.org/guide/vuex-store)


## State
> Source of Truth

``` javascript
export default () => ({
  list: []
})
```

## Actions
> API Calls + Data Actions

``` javascript
async nuxtServerInit({ commit }) {
  const { data } = this.$axios.$get('https://api.mysite.com/list')
  commit('setList', data)
}
```

## Mutations
> Receive Data from Actions to Update State

``` javascript
setList(state, payload) {
  state.list = payload
}
```

## Getters
> Retrieve State

``` javascript
getList: state => {
  return state.list
}
```
