import pkg from './package'

export default {
  mode: 'universal',

  // <head>
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }]
  },

  // Environment Variables
  env: {},

  // Progress Bar
  loading: { color: '#fff' },

  // Global Stylesheets
  css: [ '~/assets/stylesheets/main.css' ],

  // Plugins
  plugins: [],

  // Modules
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/pwa'
  ],

  // Axios module configuration
  axios: {},

  // Build Configuration
  build: {
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
          options: {
            fix: true
          }
        })
      }
    }
  }
}
