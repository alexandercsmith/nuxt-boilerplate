# nuxt-boilerplate

> Nuxt.js Boilerplate and Template App - Universal

Nuxt.js 2.8.1

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

## Features / Installations
* PWA (Progressive Web App Support)
* Linter / Formatter
* Prettier
* Axios
  * [Nuxt.js - Axios Usage](https://axios.nuxtjs.org/usage)
  * [Nuxt.js - Axios Module](https://github.com/nuxt-community/axios-module#options)


## Docs
* [Nuxt.js](https://nuxtjs.org)
* [Nuxt.js - Guide](https://nuxtjs.org/guide)
* [Nuxt.js - API](https://nuxtjs.org/api)
